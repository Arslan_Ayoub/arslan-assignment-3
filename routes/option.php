<?php

Route::get('index','Option\OptionController@index');
Route::post('add', 'Option\OptionController@add');
Route::post('edit', 'Option\OptionController@edit');
Route::post('delete', 'Option\OptionController@destroy');
