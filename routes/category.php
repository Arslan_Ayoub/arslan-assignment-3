<?php

Route::get('index','Category\CategoryController@index');
Route::post('add', 'Category\CategoryController@add');
Route::post('edit', 'Category\CategoryController@edit');
Route::post('delete', 'Category\CategoryController@destroy');
