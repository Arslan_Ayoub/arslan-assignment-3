<?php

Route::get('index','Type\TypeController@index');
Route::post('add', 'Type\TypeController@add');
Route::post('edit', 'Type\TypeController@edit');
Route::post('delete', 'Type\TypeController@destroy');
