<?php











Route::get('index','Product\ProductController@index');
Route::post('add', 'Product\ProductController@add');
Route::post('edit', 'Product\ProductController@edit');
Route::post('delete', 'Product\ProductController@destroy');
Route::post('deletePhotos', 'Product\ProductController@deletePhotos');

