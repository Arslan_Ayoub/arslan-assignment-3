<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('/update', 'User\UserController@update');

Route::POST('/login', 'User\UserController@login');
Route::post('/register', 'User\UserController@register');
Route::post('/logout', 'User\UserController@logout');
Route::post('/refresh', 'User\UserController@refresh');
Route::get('/me', 'User\UserController@me');
Route::post('/logoutFromAllDevices', 'User\UserController@logoutFromAllDevices');
Route::post('/logoutAllUsers', 'User\UserController@logoutAllUsers');
Route::post('forgot-password', 'User\NewPasswordController@forgotPassword');
Route::post('reset-password', 'User\NewPasswordController@reset')->name('reset-password');
Route::get('password.reset', 'User\NewPasswordController@passwordReset')->name('password.reset');


Route::get('/test', 'User\NewPasswordController@test')->name('test');

Route::POST('upload', 'User\S3Controller@upload');


Route::POST('store','User\S3Controller@store');
