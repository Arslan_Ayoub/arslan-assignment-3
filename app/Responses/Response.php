<?php

namespace App\Responses;

class Response
{


    public static function result($status, $message, $data)
    {
        return response()->json(['status' => $status, 'message' => $message, 'result' => ['data' => $data]]);
    }

}
