<?php

namespace App\Listeners;

use App\Events\RegistrationMail;
use App\Mail\UserRegistrationMail;
use App\Models\User;


use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRegistrationMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  RegistrationMail  $event
     * @return void
     */
    public function handle(RegistrationMail $event)
    {
        $details = [
            'name' => 'Welcome '. $event->user['name'],
            'subject' => 'you have Been Successfully Registered..!!!'
        ];


        Mail::to($event->user['email'])->send(new UserRegistrationMail($details));
    }
}
