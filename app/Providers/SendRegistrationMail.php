<?php

namespace App\Providers;

use App\Providers\RegistrationMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRegistrationMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationMail  $event
     * @return void
     */
    public function handle(RegistrationMail $event)
    {
        //
    }
}
