<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class Add extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'name' => 'required|string|min:2',
            'description' => 'required|string|min:2',
            'cost_price' => 'required|integer',
            'sale_price' => 'required|integer',
            'option_id' => 'required|integer|exists:App\Models\Option,id|integer',
            'category_id' => 'required|integer|exists:App\Models\Category,id|integer',
            'type_id' => 'required|integer|exists:App\Models\Type,id|integer',
        ];
    }
}
