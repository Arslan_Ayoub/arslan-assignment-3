<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\S3Controller;
use App\Http\Requests\Product\Add;
use App\Http\Requests\Product\Delete;
use App\Http\Requests\Product\DeletePhotos;

use App\Http\Requests\Product\Edit;
use App\Models\Image;
use App\Models\Product;

use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;

use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return Response::result(true, "All Products", $products);
    }

    public function add(Add $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];

        $product = Product::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'cost_price' => $request['cost_price'],
            'sale_price' => $request['sale_price'],
            'option_id' => $request['option_id'],
            'category_id' => $request['category_id'],
            'type_id' => $request['type_id'],
            'created_by' => $userId,
        ]);

        $files = $request->file('image');
       return S3Controller::AddProductPic($files,$product);


    }

    public function edit(Edit $request)
    {

        $files = $request->file('image');
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];
        $productId = $request['id'];
        $product = Product::find($productId);
         DB::table('products')->where('id', $productId)->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'cost_price' => $request['cost_price'],
            'sale_price' => $request['sale_price'],
            'option_id' => $request['option_id'],
            'category_id' => $request['category_id'],
            'type_id' => $request['type_id'],
            'updated_by' => $userId,
        ]);


        return S3Controller::UpdateProductPic($files, $product);
    }

    public function destroy(Delete $request)
    {


        $productId = $request['id'];
        $product = Product::find($productId);
        if ($product != null) {
            $ids = Image::select('id')->where('model_type','App\Models\Product')->where('model_id',$productId)->get();

            S3Controller::deleteProductPhotos($ids);

            $product->delete();
            return Response::result(true, "Product Deleted Successfully", null);
        } else {
            return Response::result(false, "Product Not Exists", null);
        }
    }

    public function deletePhotos(Request $request)
    {
        $ids = $request['id'];
        foreach ($ids as $id) {
            if (Image::where('id', $id)->exists()) {
                $product = Image::find($id);
                $imageName = $product['file_name'];
                $status = Storage::disk('s3')->delete($imageName);
                if ($status) {
                    $product->delete();
                }
            }
        }
        return Response::result(true, "Product Photo Deleted successfully", null);
    }


}
