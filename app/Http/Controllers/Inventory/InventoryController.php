<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller
{

    public function index()
    {
        $inventory = DB::table('product_view')->get();
        return Response::result('true', 'All Products in Inventory', $inventory);
    }

}
