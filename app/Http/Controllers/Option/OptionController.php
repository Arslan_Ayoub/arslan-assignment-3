<?php

namespace App\Http\Controllers\Option;

use App\Http\Controllers\Controller;
use App\Http\Requests\Option\Add;
use App\Http\Requests\Option\Delete;
use App\Http\Requests\Option\Edit;
use App\Models\Option;
use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;


class OptionController extends Controller
{

    public function index()
    {
        $option = Option::all();
        return Response::result(true, "All Options", $option);
    }

    public function add(Add $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];

        $option = Option::create([
            'color' => $request['color'],
            'size' => $request['size'],
            'created_by' => $userId
        ]);
        return Response::result(true, "Option Added Successfully", $option);
    }

    public function edit(Edit $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];
        $optionId = $request['id'];
        DB::table('options')->where('id', $optionId)->update([
            'color' => $request['color'],
            'size' => $request['size'],
            'updated_by' => $userId,
        ]);
        $optionsData = $request->all();
        return Response::result(true, "Option Updated Successfully", $optionsData);
    }

    public function destroy(Delete $request)
    {
        $optionId = $request['id'];
        $option = Option::find($optionId);
        if ($option != null) {
            $option->products()->update(['option_id' => null]);
            $option->delete();
            return Response::result(true, "Option Deleted Successfully", null);
        } else {
            return Response::result(false, "Option Not Exists", null);
        }
    }
}
