<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\Add;
use App\Http\Requests\Category\Delete;
use App\Http\Requests\Category\Edit;
use App\Models\Category;
use App\Responses\Response;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;


class CategoryController extends Controller
{

    public function index()
    {

        $categories = Category::all();
        return Response::result(true, "All Categories", $categories);
    }

    public function add(Add $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];
        $category = Category::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => $userId
        ]);
        return Response::result(true, "Category Added Successfully", $category);
    }

    public function edit(Edit $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];
        $categoriesId = $request['id'];
        DB::table('categories')->where('id', $categoriesId)->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'updated_by' => $userId,
        ]);
        $categoriesData = $request->all();
        return Response::result(true, "Category Updated Successfully", $categoriesData);
    }

    public function destroy(Delete $request)
    {

        $categoryId = $request['id'];
        $category = Category::find($categoryId);

        if ($category != null) {


            $category->products()->update(['category_id' => null]);

            $category->delete();
            return Response::result(true, "Category Deleted Successfully", null);
        } else {
            return Response::result(false, "Category Not Exists", null);
        }
    }
}
