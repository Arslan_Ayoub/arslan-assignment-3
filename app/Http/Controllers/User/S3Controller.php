<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\DocBlockFactory;
use Tymon\JWTAuth\Facades\JWTAuth;

class S3Controller extends Controller

{

    public static function store($request)
    {

        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);

        $file = $request->file('image');
        $file_name = time() . $file->getClientOriginalName();
        $filePath = 'arslan-images/' . $file_name;


        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file), 'public');

        if ($status) {
            $imageurl = $s3->url($filePath);
            $user->images()->updateOrCreate(
                [
                    'model_id' => $user->id,
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $imageurl,
                ]);
            $user['image_url'] = $imageurl;

            return Response::result(true, "Picture Uploaded successfully", $user);

        } else {
            return Response::result(true, "Pic is not Uploaded", null);

        }
    }

    public static function userProfilePic($userId)
    {
        $profilePic = Image::select('file_name')->where('model_id', $userId)->where('model_type', 'App\Models\User')->first();
        return 'https://dev-sajid.s3.amazonaws.com/' . $profilePic['file_name'];

    }

    public static function updateUserProfilePic($file,$user){

        $file_name = time() . $file->getClientOriginalName();
        $filePath = 'arslan-images/' . $file_name;


        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file), 'public');

        if ($status) {
            $imageurl = $s3->url($filePath);
            $user->images()->updateOrCreate(
                [
                    'model_id' => $user['id'],
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $filePath,
                ]);
            $user['image_url'] = $imageurl;

            return Response::result(true, "User Updated successfully", $user);

        } else {
            return Response::result(false, "Pic is not Updated", null);

        }
    }

    public static function AddProductPic($files,$product){

        foreach ($files as $file) {

            $file_name = time() . $file->getClientOriginalName();
            $filePath = 'arslan-product-images/' . $file_name;

            $s3 = Storage::disk('s3');
            $status = $s3->put($filePath, file_get_contents($file), 'public');

            if ($status) {
                $imageurl = $s3->url($filePath);

                $product->images()->insert(
                    [
                        'model_id' => $product['id'],
                        'model_type' => 'App\Models\Product',
                        'file_name' => $filePath,
                    ]);
                $product['image_url' . time()] = $imageurl;

            }

        }
        return Response::result(true, "Product Added successfully", $product);
    }

    public static function UpdateProductPic($files , $product){

        foreach ($files as $file) {

            $file_name = time() . $file->getClientOriginalName();
            $filePath = 'arslan-product-images/' . $file_name;

            $s3 = Storage::disk('s3');
            $status = $s3->put($filePath, file_get_contents($file), 'public');

            if ($status) {
                $imageurl = $s3->url($filePath);

                $product->images()->insert(
                    [
                        'model_id' => $product['id'],
                        'model_type' => 'App\Models\Product',
                        'file_name' => $filePath,
                    ]);
                $product['image_url' . time()] = $imageurl;

            }

        }
        return Response::result(true, "Product Updated successfully", $product);
    }


    public static function deleteProductPhotos($ids)
    {

        foreach ($ids as $id) {

            if (Image::where('id', $id['id'])->exists()) {
                $product = Image::find($id['id']);
                $imageName = $product['file_name'];
                $status = Storage::disk('s3')->delete($imageName);

                if ($status) {
                    $product->delete();
                }
            }
        }
    }


}
