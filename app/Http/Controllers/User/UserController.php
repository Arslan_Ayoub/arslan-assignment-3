<?php

namespace App\Http\Controllers\User;

use App\Events\RegistrationMail;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Add;
use App\Http\Requests\User\Edit;
use App\Http\Requests\User\Login;
use App\Mail\UserRegistrationMail;

use DeviceDetector\Parser\Client\Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Responses\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\False_;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');

    }

    //Welcome Page Display at / route
    public function index()
    {
        return view('welcome');
    }

    //Login using custom validation

    public function login(Login $request)
    {
        $browserDetails = $request->userAgent();
        $user = User::where('email', $request['email'])->first();

        $browser = DB::table('user_token')->where('user_id', $user->id)->where('browser_detail', $browserDetails)->where('status', 1)->get();
        $profilePic = S3Controller::userProfilePic($user->id);
        if ($browser->first() == null) {
            $credentials = $request->only('email', 'password');
            try {
                $token = JWTAuth::attempt($credentials);
            } catch (JWTException $e) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }

            DB::table('user_token')
                ->insert([
                    'user_id' => $user->id,
                    'created_by' => $user->id,
                    'token' => $token,
                    'browser_detail' => $browserDetails,
                ]);
            $user['token'] = $token;
            $user['Profile Pic'] = $profilePic;
            return Response::result(true, 'User login', $user);


        } else {

            $user['Profile Pic'] = $profilePic;
            $user['token'] = $browser[0]->token;
            return Response::result(true, 'Already Login', $user);
        }

    }

    //This function will return the information about the logged in user

    public function me()
    {

        $user = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::parseToken()->getToken();
        return Response::result(true, 'User Data', $user);
    }

    //this function will take the token in header and then invalidate it
    public function logout()
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::parseToken()->authenticate();

        try {
            JWTAuth::parseToken()->authenticate();
            JWTAuth::invalidate(JWTAuth::parseToken()->getToken());

        } catch (JWTException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }


        DB::table('user_token')->where('token', $token)->update(['status' => 0]);

        return Response::result(true, 'User Logged Out SuccessFully', null);
    }

    //function create new token
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
        ]);
    }


    public function guard()
    {
        return Auth::guard();
    }

    //register mew user using custom validation
    public function register(Add $request)
    {

        $browserDetails = $request->userAgent();

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'address' => $request['address'],
            'phone' => $request['phone'],
            'password' => Hash::make($request->get('password')),
        ]);
        $token = JWTAuth::fromUser($user);
        DB::table('user_token')
            ->insert([
                'user_id' => $user['id'],
                'created_by' => $user['id'],
                'token' => $token,
                'browser_detail' => $browserDetails,
            ]);


        event(new RegistrationMail($user));


        $file = $request->file('image');
        $file_name = time() . $file->getClientOriginalName();
        $filePath = 'arslan-images/' . $file_name;


        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file), 'public');

        if ($status) {
            $imageurl = $s3->url($filePath);
            $user->images()->updateOrCreate(
                [
                    'model_id' => $user['id'],
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $filePath,
                ]);
            $user['image_url'] = $imageurl;

            return Response::result(true, "User Registered successfully", $user);

        } else {
            return Response::result(true, "Pic is not Uploaded", null);

        }
    }

    //function will logout the current user from all the devices
    public function logoutFromAllDevices()
    {

        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];

        $userInfo = DB::table('user_token')->where('user_id', $userId)->where('status', 1)->get();

        foreach ($userInfo as $info) {

            JWTAuth::invalidate($info->token);
            DB::table('user_token')->where('token', $info->token)->update(['status' => 0]);

        }
        return Response::result(true, 'User Logged Out From All Devices', null);


    }

    //function logout all the active users
    public function logoutAllUsers()
    {

        $userInfo = DB::table('user_token')->where('status', 1)->get();

        foreach ($userInfo as $info) {

            JWTAuth::invalidate($info->token);
            DB::table('user_token')->where('token', $info->token)->update(['status' => 0]);

        }
        return Response::result(true, 'All Users SuccessFully LoggedOut', null);


    }

    public function update(Edit $request)
    {

        $user = JWTAuth::parseToken()->authenticate();

        User::where('id', $user['id'])->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'address' => $request['address'],
            'phone' => $request['phone'],
            'password' => Hash::make($request->get('password')),
            'updated_by' => $user['id']
        ]);

        $file = $request->file('image');

        return S3Controller::updateUserProfilePic($file, $user);


    }


}
