<?php

namespace App\Http\Controllers\Type;

use App\Http\Controllers\Controller;
use App\Http\Requests\Type\Add;
use App\Http\Requests\Type\Delete;
use App\Http\Requests\Type\Edit;
use App\Models\Type;

use App\Responses\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;


class TypeController extends Controller
{

    public function index()
    {
        $type = Type::all();
        return Response::result(true, "All Types", $type);
    }

    public function add(Add $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];

        $type = Type::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by' => $userId
        ]);
        return Response::result(true, "Type Added Successfully", $type);
    }

    public function edit(Edit $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $userId = $user['id'];
        $typeId = $request['id'];
        DB::table('types')->where('id', $typeId)->update([
            'name' => $request['name'],
            'description' => $request['description'],
            'updated_by' => $userId,
        ]);
        $typeData = $request->all();
        return Response::result(true, "Type Updated Successfully", $typeData);
    }

    public function destroy(Delete $request)
    {
        $typeId = $request['id'];
        $type = Type::find($typeId);
        if ($type != null) {
            $type->products()->update(['type_id' => null]);
            $type->delete();
            return Response::result(true, "Type Deleted Successfully", null);
        } else {
            return Response::result(false, "Type Not Exists", null);
        }
    }
}
