<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DailyNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send a reminder notification daily to all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $notification = 'Hey There, We have lot of new products in our store. just visit there... Hurry Up...!!!';



        $users = User::all();
        foreach ($users as $user) {
            Mail::to($user['email'])->send(new \App\Mail\DailyNotification());



//            Mail::raw($notification, function ($mail) use ($user) {
//                $mail->from('arslan.a@ovada.com');
//                $mail->to($user->email)
//                    ->subject('Daily New Products Reminder!!!')
//                ->view('mail.dailyNotification');
//            });
        }

        $this->info('Successfully sent daily notification to everyone.');






    }
}
