<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use Notifiable;
    use Softdeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description','cost_price','sale_price','option_id','category_id','type_id','created_by','updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_by','updated_by','updated_at','created_at','deleted_at'
    ];


    public function categories(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function types(){
        return $this->belongsTo(Type::class,'type_id');
    }
    public function options(){
        return $this->belongsTo(Option::class,'option_id');
    }
    public function images()
    {
        return $this->morphOne(Image::class, 'model');
    }


}
