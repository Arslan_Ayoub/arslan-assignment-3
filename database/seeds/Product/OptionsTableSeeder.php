<?php

use App\Models\Option;
use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $options = [
                [ 'color' => 'red' , 'size' =>  'S' ],
                [ 'color' => 'red' , 'size' =>  'M' ],
                [ 'color' => 'red' , 'size' =>  'L' ],
                [ 'color' => 'red' , 'size' =>  'XL' ],

                [ 'color' => 'green' , 'size' =>  'S' ],
                [ 'color' => 'green' , 'size' =>  'M' ],
                [ 'color' => 'green' , 'size' =>  'L' ],
                [ 'color' => 'green' , 'size' =>  'XL' ],

                [ 'color' => 'blue' , 'size' =>  'S' ],
                [ 'color' => 'blue' , 'size' =>  'M' ],
                [ 'color' => 'blue' , 'size' =>  'L' ],
                [ 'color' => 'blue' , 'size' =>  'XL' ],

                [ 'color' => 'black' , 'size' =>  'S' ],
                [ 'color' => 'black' , 'size' =>  'M' ],
                [ 'color' => 'black' , 'size' =>  'L' ],
                [ 'color' => 'black' , 'size' =>  'XL' ],
            ];

            Option::insert($options);
    }
}
