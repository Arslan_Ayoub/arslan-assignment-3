<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ProductView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("
      CREATE
        OR REPLACE View product_view as
             SELECT
              products.name as product_name,
              products.description as product_description,
              categories.name as category,
              	types.name as type,
							options.color,
							options.size,
							 products.sale_price ,
              products.cost_price
            FROM products
            JOIN categories
              ON products.category_id = categories.id AND categories.deleted_at is null AND products.category_id IS NOT NULL
							JOIN options
            ON products.option_id = options.id AND options.deleted_at is null AND products.option_id IS NOT NULL
						JOIN types
              ON products.type_id = types.id AND types.deleted_at is null AND products.type_id IS NOT NULL
							where products.deleted_at is null;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_view');
    }
}
