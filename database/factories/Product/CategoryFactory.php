<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->bothify('?????##'),
        'description' => $faker->text,
        'created_by' => $faker->numberBetween($min=1,$max=10),
    ];
});
