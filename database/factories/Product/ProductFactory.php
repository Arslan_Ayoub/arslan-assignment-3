<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->bothify('?????##'),
        'description' => $faker->text,
        'cost_price' => $faker->numerify('###'),
        'sale_price' => $faker->numerify('###'),
        'type_id' => $faker->numberBetween($min=1,$max=10),
        'category_id' => $faker->numberBetween($min=1,$max=10),
        'option_id' => $faker->numberBetween($min=1,$max=10),
        'created_by' => $faker->numberBetween($min=1,$max=10),
    ];

});
