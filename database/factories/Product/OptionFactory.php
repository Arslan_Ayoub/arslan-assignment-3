<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Option;
use Faker\Generator as Faker;

$factory->define(Option::class, function (Faker $faker) {
    return [
        'color' => $faker->bothify('?????##'),
        'size' => $faker->text,
        'created_by' => $faker->numberBetween($min=1,$max=10),
    ];
});
